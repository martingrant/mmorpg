#pragma once

#include <SDL.h>

class Entity
{
public:
	Entity(int x, int y, int w, int h) 
	{
		rect.x = x;
		rect.y = y;
		rect.w = w;
		rect.h = h;
	}
	~Entity() { }

	SDL_Rect rect;

private:

};