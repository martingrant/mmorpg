#include <iostream>

#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <SDL_ttf.h>

#include "SDL_net.h"

#include "ClientSocket.h"

#include "Entity.h"

SDL_Window* window;
SDL_Renderer* renderer;
SDL_Event SDLEvent;
bool running;

Entity* player;
Entity* other;

ClientSocket *clientSocket;


bool getKeyState(const char* key)
{
	bool status = false;

	//char* scanCode = strcat("SDL_Scancode", key);

	const Uint8* keyboard = SDL_GetKeyboardState(NULL);

	if (keyboard[SDL_GetScancodeFromName(key)]) {
		status = true;
		//std::cout << "Key pressed: " << SDL_GetScancodeName(scanCode) << " (Scancode: " << scanCode << ")" << std::endl; 
	}

	return status;
}

void renderRect(const SDL_Rect* rect, Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha)
{
	SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);
	SDL_RenderFillRect(renderer, rect);
	SDL_RenderDrawRect(renderer, rect);
	SDL_SetRenderDrawColor(renderer, 100, 10, 50, SDL_ALPHA_OPAQUE);
}


void update()
{
	SDL_PollEvent(&SDLEvent);

	if (getKeyState("W")) player->rect.y -= 1;
	if (getKeyState("S")) player->rect.y += 1;
	if (getKeyState("A")) player->rect.x -= 1;
	if (getKeyState("D")) player->rect.x += 1;


	std::string msg = clientSocket->receivedMessage;

	if (msg.length() >= 3)
	{
		std::string x = msg.substr(0, msg.find(","));
		std::string y = msg.substr(msg.find(",") + 1, msg.length());

		std::cout << "x" << x << " y" << y << std::endl;

		int posx = stoi(x);
		int posy = stoi(y);

		other->rect.x = posx;
		other->rect.y = posy;

		const Uint8 *state = SDL_GetKeyboardState(NULL);
		if (state[SDL_SCANCODE_RETURN]) {
			printf("<RETURN> is pressed.\n");
		}
		if (state[SDL_SCANCODE_RIGHT] && state[SDL_SCANCODE_UP]) {
			printf("Right and Up Keys Pressed.\n");
		}

	}
}


void render()
{
	SDL_RenderClear(renderer);

	renderRect(&player->rect, 100, 100, 200, 100);

	renderRect(&other->rect, 100, 200, 200, 100);

	SDL_RenderPresent(renderer);
}


void initnetwork()
{
	// Initialise SDL_net (Note: We don't initialise or use normal SDL at all - only the SDL_net library!)
	if (SDLNet_Init() == -1)
	{
		std::cerr << "Failed to intialise SDL_net: " << SDLNet_GetError() << std::endl;
		exit(-1);
	}

	// Create a pointer to a ServerSocket object

	try
	{
		// Now try to instantiate the client socket
		// Parameters: server address, port number, buffer size (i.e. max message size)
		// Note: You can provide the serverURL as a dot-quad ("1.2.3.4") or a hostname ("server.foo.com")
		std::cout << "Welcome. Enter hostname: ";
		std::string hostName = "localhost";
		//std::cin >> hostName;

		clientSocket = new ClientSocket(hostName, 1234, 512);


		// Attempt to connect to the server at the provided address and port
		clientSocket->connectToServer();

		

		cout << "Use /quit to disconnect or /shutdown to shutdown the server." << endl;

		// Display the initial prompt
		clientSocket->displayPrompt();


	}
	catch (SocketException e)
	{
		std::cerr << "Something went wrong creating a ClientSocket object." << std::endl;
		std::cerr << "Error is: " << e.what() << std::endl;
		std::cerr << "Terminating application." << std::endl;
		exit(-1);
	}
}


void updatenetwork()
{
	
		// Run the main loop...
		if (((clientSocket->getShutdownStatus() == false)))
		{

			string receivedMessage = "";

			// Check if we've received a message
			receivedMessage = clientSocket->checkForIncomingMessages();

			// If so then...
			if (receivedMessage != "")
			{
				// Display the message and then blank it...
				clientSocket->displayMessage(receivedMessage);

				// and then re-display the prompt along with any typed-but-not-yet-sent input
				clientSocket->displayPrompt();
			}

			// Get and deal with input from the user in a non-blocking manner
			clientSocket->getUserInput();

			clientSocket->send(player->rect.x, player->rect.y);

			// ... until we decide to quit or the server is shut down
		} 

	

}


int main(int argc, char *args[])
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cout << "SDL_Init: " << SDL_GetError() << std::endl;
	}

	running = true;

	Uint32 windowFlags = SDL_WINDOW_SHOWN;

	window = SDL_CreateWindow("MMORPG", SDL_WINDOWPOS_CENTERED_DISPLAY(0), SDL_WINDOWPOS_CENTERED_DISPLAY(0), 800, 600, windowFlags);

	if (window == NULL)
	{
		std::cout << "SDL_CreateWindow: " << SDL_GetError() << std::endl;
	}

	SDL_GL_SetSwapInterval(1);

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(renderer, 100, 10, 50, SDL_ALPHA_OPAQUE);

	player = new Entity(0, 0, 300, 300);
	other = new Entity(300, 400, 50, 50);

	initnetwork();
	

	while (running)
	{
		updatenetwork();
		update();
		render();
	}

	return 0;
}